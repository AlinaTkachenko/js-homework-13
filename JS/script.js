/*1. setTimeout дозволяє нам запускати функцію один раз через певний інтервал часу,а setInterval дозволяє нам запускати функцію багаторазово,
починаючи через певний інтервал часу і кожний новий визов функції буде через цей інтервал часу.*/
/*2. Якщо передати до setTimeout нульову затримку то функція відпрацює одразу після завершення виконання поточного скрипту, 
тобто якщо після setTimeout наприклад буде consol.log() то спочатку відображаеться consol.log а потім setTimeout.
Чому так, бо setTimeout спочатку попадає до call stack потім web apis(де знаходиться поки йде виконання поточного скрипту та поки йде заданий інтервал)
потім callback queue потім call stack*/
/*Функція посилається на зовнішнє лексичне середовище, тому, поки вона живе, зовнішні змінні також живуть.
Вони можуть зайняти набагато більше пам’яті, ніж сама функція. Тому, коли нам більше не потрібна запланована функція, краще її скасувати, навіть якщо вона дуже мала.*/

const img = document.querySelector('img');
const btnStop = document.querySelector('.stop');
const btnStart = document.querySelector('.start');
let srcImg = ['./img/1.jpg', './img/2.jpg', './img/3.JPG', './img/4.png'];
let count = 1;
let timerId = setInterval(showPicture, 3000);

function showPicture() {
    img.removeAttribute('src');
    img.setAttribute('src', `${srcImg[count]}`);

    if (count < 3) {
        count++;
    } else {
        count = 0;
    }
};

btnStop.addEventListener('click', function () {
    clearInterval(timerId);
});

btnStart.addEventListener('click', function () {
    count = srcImg.indexOf(img.getAttribute('src'));
    timerId = setInterval(showPicture, 3000);
});